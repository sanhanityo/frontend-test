Frontend Test
===================
Front End Developer test for clapping ape.

### Installation
>Untuk preview file yang sudah di bundle bisa lihat di directory **/dist**.

Pastikan anda memiliki Node.js v6+
Install dependencies, devDependencies.

```sh
$ cd frontend-test
$ npm install
$ gulp build // build file
$ gulp watch // command auto build ketika save file
```

### Library
  - Zurb Foundation
  - Swiper.js
  - Webfontloader