window.$ = window.jQuery = require('jquery');
window.Swiper = require('swiper');
window.WebFont = require('webfontloader');

WebFont.load({
    google: {
        families: ['Open Sans:300,400,600']
    }
});

$(function() {
    $('.js-open-menu').click(function() {
        $('body').toggleClass('overflow-hidden');
        $(this).toggleClass('is-open');
        $('.mobile-menu').toggleClass('is-open');
    });

    var heroSlider = new Swiper('.js-hero', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    });
})